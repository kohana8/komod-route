const {K8, Controller} = require('@komino/k8');
const FastifyController = require('./FastifyController');

class RouteList{
  /**
   *
   * @param {string} path
   * @param {string | Controller} controller
   * @param {string} action
   * @param {string} method
   * @param {number} weight
   */

  static add(path, controller, action='index', method= RouteList.methods.GET, weight=5){
    RouteList.routes[weight] = RouteList.routes[weight] || [];

    RouteList.routes[weight].push({
      path: path,
      controller : controller,
      action : action,
      method : method
    });
  }

  static stub(path, message){
    RouteList.routes[0] = RouteList.routes[0] || [];
    RouteList.routes[0].push({
      path: path,
      message : message
    })
  }

  static createRoute(app){
    RouteList.routes.flat().forEach(x => {
      //route: {
      //       path: path,
      //       controller : controller, {string | Controller}}
      //       action : action,
      //       method : method
      //}
      //route : {
      //        path: path,
      //        message: message
      //}

      //guards
      if (!x) return;
      //simply return stub message
      if (x.message)return app.get(x.path, async () => x.message);

      const callback = async(request, reply) => {
        K8.validateCache();
        request.params.action = x.action;
        request.params.controller = (typeof x.controller === 'string') ? x.controller : x.controller.name;

        const {resolveRoute} = K8.require('helper/HelperRoute');
        const controller = (typeof x.controller === 'string') ? K8.require(x.controller) : x.controller;

        FastifyController.prepare(
          reply,
          await resolveRoute(controller, request)
        );
      };

      switch (x.method) {
        case RouteList.methods.GET:
          app.get(x.path, callback);
          break;
        case RouteList.methods.POST:
          app.post(x.path, callback);
          break;
        case RouteList.methods.PUT:
          app.put(x.path, callback);
          break;
        case RouteList.methods.DELETE:
          app.delete(x.path, callback);
          break;
      }
    });
  }
}

RouteList.routes = [];
RouteList.methods = {
  CONNECT : 'CONNECT',
  DELETE : 'DELETE',
  GET : 'GET',
  HEAD : 'HEAD',
  OPTIONS : 'OPTIONS',
  PATCH : 'PATCH',
  POST : 'POST',
  PUT : 'PUT',
  TRACE : 'TRACE'
};

module.exports = RouteList;