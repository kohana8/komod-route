const prepare = (reply, result) => {
  //set cookie if fastify-cookie loaded
  if(!!reply.setCookie){
    result.cookies.forEach(cookie => reply.setCookie(cookie.name, cookie.value, cookie.options));
  }

  Object.keys(result.headers).forEach(headerName => {
    reply.header(headerName, result.headers[headerName]);
  });
  reply.code(result.status);
  reply.send(result.body);
};


module.exports = {
  handlerFactory : Controller => async (request, reply) => {
    prepare(reply, await new Controller(request).execute())
  },

  prepare : prepare
};