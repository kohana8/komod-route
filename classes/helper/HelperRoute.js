module.exports = {
  resolveRoute : async(Controller, request) => {
    return await new Controller(request).execute();
  }
};