const {K8} = require('@komino/k8');
K8.addNodeModules(require.resolve('./'));

module.exports = {
  RouteList: require('./classes/RouteList'),
  HelperRoute: require('./classes/helper/HelperRoute'),
  FastifyController: require('./classes/FastifyController'),
};